#ifndef TEMPERATURE_SENSOR_H
#define TEMPERATURE_SENSOR_H
/**
 *******************************************************************************
 * @file TemperatureSensor.h
 *******************************************************************************
 */
/* Constants */

/* Types */
typedef enum {
    TEMP_OK = 0,
    TEMP_ERROR_SHORT_VCC = 0x04,
    TEMP_ERROR_SHORT_GND = 0x02,
    TEMP_ERROR_OPEN = 0x01,
} TempMsg_t;

/* Functions */
TempMsg_t ReadTemperature(float *thermocouple, float *junction);

/******************************************************************************/
#endif /* ifndef TEMPERATURE_SENSOR_H */
