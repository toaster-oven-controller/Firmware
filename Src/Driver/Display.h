#ifndef DISPLAY_H
#define DISPLAY_H
/**
 *******************************************************************************
 * @file    Display.h
 *******************************************************************************
 */
/* Constants */
/* Special Symbols */
enum {
    SYM_DEG = 0x80,
    SYM_ARROW_LEFT = 0xE1,
    SYM_DOT = 0xBB,
};

/* Types */
typedef enum {
    DISP_4_BIT = 0x02,
    DISP_CLEAR = 0x01,
    DISP_CURSOR_OFF = 0x0C,
    DISP_CURSOR_BLINK = 0x0F,
    DISP_CURSOR_AUTO_INC = 0x02,
    DISP_RE_ENABLE = 0x24,
    DISP_RE_DISABLE = 0x20,
    DISP_FOUR_LINE = 0x09,
    DISP_CURSOR_POS = 0x80,
} DisplayCmd_t;

/* Functions */
void InitDisplay(void);
void DisplayWriteCharacter(char character);
void DisplayWriteString(const char *str);
void DisplayPrintf(const char *fmt, ...);
void DisplayClear(void);
void DisplaySetCursor (int y, int x);

/******************************************************************************/
#endif
