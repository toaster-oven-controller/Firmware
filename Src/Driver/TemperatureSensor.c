/**
 *******************************************************************************
 * @file    TemperatureSensor.c
 * @brief   Reads the thermocouple temperature from the MAX31855
 * Thermocouple-to-Digital Converter
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include "TemperatureSensor.h"
#include "stm32f1xx_hal.h"

/* Private variables */

/* Private functions */

/******************************************************************************/
/**
 * @brief Reads the temperature data from the MAX31855 over SPI
 * @param thermocouple Temperature of the thermocouple in °C
 * @param junction Temperature of the IC junction in °C
 * @return Error message
 */
TempMsg_t ReadTemperature(float *thermocouple, float *junction) {
    uint8_t dummyData[4];
    uint8_t data[4];

    SPI_HandleTypeDef *hspi = GetTempSenseHandle();
    HAL_GPIO_WritePin(TEMP_CS_GPIO_Port, TEMP_CS_Pin, 0);
    /* Use transmit and receive because receive only has a bug */
    HAL_SPI_TransmitReceive(hspi, dummyData, data, 4, 1000);
    while (hspi->State == HAL_SPI_STATE_BUSY);
    HAL_GPIO_WritePin(TEMP_CS_GPIO_Port, TEMP_CS_Pin, 1);

    /* thermocouple temperature */
    *thermocouple = (((data[0] & 0x7F) << 6)
                     | ((data[1] & 0xFC) >> 2)) * 0.25f;
    // evaluate sign bit
    if (data[0] & 0x80) {
        *thermocouple *= -1.0f;
    }

    /* IC junction temperature */
    *junction = (((data[2] & 0x7F) << 4)
                 | ((data[3] & 0xF0) >> 4)) * 0.0625f;
    // evaluate sign bit
    if (data[2] & 0x80) {
        *junction *= -1.0f;
    }

    /* Errors define in tempMsg_t do directly correlate to fault flags */
    return data[3] & 0x07;
}

/******************************************************************************/