/**
 *******************************************************************************
 * @file    UsbCom.c
 * @brief   Communication with a Virtual COM port over USB (CDC)
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include "UsbCom.h"
#include "stm32f1xx_hal.h"
#include "usbd_cdc_if.h"
#include <stdarg.h>
#include <string.h>

/* Private variables */

/* Private functions */

/******************************************************************************/
/**
 * @brief Assembles string an arguments and sends it to the display
 * @param fmt string
 * @param argp arguments
 */
static void vprint(const char *fmt, va_list argp) {
    char string[50];
    if (0 < vsprintf(string, fmt, argp)) // build string
    {
        CDC_Transmit_FS(string, strlen(string));
    }
}

/**
 * @brief printf function for the Virtual COM port over USB
 * @param fmt string
 * @param ... variables
 *
 * Source: https://electronics.stackexchange.com/questions/206113/
 * how-do-i-use-the-printf-function-on-stm32
 */
void UsbPrintf(const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}

/**
 * @brief Prints a ASCII string onto the Virtual COM port
 * @param str pointer to a string
 */
void UsbPrintString(char *str) {
    CDC_Transmit_FS(str, strlen(str));
}
/******************************************************************************/