#ifndef BEEPER_H
#define BEEPER_H
/**
 *******************************************************************************
 * @file Beeper.h
 *******************************************************************************
 */
/* Constants */

/* Types */

/* Functions */
void StartBeeper(int onTime, int offTime);
void StopBeeper(void);

/******************************************************************************/
#endif /* ifndef BEEPER_H */
