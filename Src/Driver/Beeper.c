/**
 *******************************************************************************
 * @file    Beeper.c
 * @brief   
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include "cmsis_os.h"
#include "stm32f1xx_hal.h"
#include "Beeper.h"

/* Private variables */
static TaskHandle_t xBeeperHandle = NULL;
static BaseType_t xBeeper;

/* Private functions */
static void vBeeper(void *pvParameters);

/******************************************************************************/
/**
 * @brief Starts the beeper task
 * @param onTime beeper on in ms
 * @param offTime beeper off in ms
 */
void StartBeeper(int onTime, int offTime) {
    static uint32_t params[2];
    params[0] = onTime;
    params[1] = offTime;

    xBeeper = xTaskCreate(
            vBeeper,
            "Beeper",
            64,
            (void *) params,
            1,
            &xBeeperHandle);
}

/**
 * @brief Stops the beeper
 */
void StopBeeper(void) {
    if (xBeeperHandle == NULL) {
        return;
    }
    vTaskDelete(xBeeperHandle);
    HAL_TIM_PWM_Stop(GetPwmHandle(), 0); /* disable buzzer */
    xBeeperHandle = NULL;
}

/**
* @brief Runs the beeper
* @param pvParameters array containing on time and off time, both in ms
*/
static void vBeeper(void *pvParameters) {
    uint32_t *params = (uint32_t *)(pvParameters);
    uint32_t onTime = params[0];
    uint32_t offTime = params[1];

    while (1) {
        HAL_TIM_PWM_Start(GetPwmHandle(), 0); /* enable buzzer */
        vTaskDelay(onTime);
        HAL_TIM_PWM_Stop(GetPwmHandle(), 0); /* disable buzzer */
        vTaskDelay(offTime);
    }
}

/******************************************************************************/