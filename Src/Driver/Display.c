/**
 *******************************************************************************
 * @file    Display.c
 * @brief   Driver for the EA DIP204B-4NLW 4x20 character display
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <stm32f103xb.h>
#include <stdarg.h>
#include <cmsis_os.h>
#include "stm32f1xx_hal.h"
#include "Display.h"

/* Private variables */

/* Private functions */
static void SimpleWait(int steps);
static void DiplayClkStroke(void);
static void DisplayWriteNibble(DisplayCmd_t data);
static void DisplayWriteCommand(DisplayCmd_t data);
static void vprint(const char *fmt, va_list argp);

/******************************************************************************/
/**
 * @brief Initializes display with 4 bit data length, no cursor
 */
void InitDisplay(void) {
    HAL_GPIO_WritePin(DISP_RST_GPIO_Port, DISP_RST_Pin, 1);
    vTaskDelay(1);
    HAL_GPIO_WritePin(DISP_RST_GPIO_Port, DISP_RST_Pin, 0);
    vTaskDelay(1);
    DisplayWriteCommand(DISP_4_BIT);
    DisplayWriteCommand(DISP_RE_ENABLE);
    DisplayWriteCommand(DISP_FOUR_LINE);
    DisplayWriteCommand(DISP_RE_DISABLE);
    DisplayWriteCommand(DISP_CURSOR_OFF);
    DisplayWriteCommand(DISP_CURSOR_AUTO_INC);
    DisplayWriteCommand(DISP_CLEAR);
    vTaskDelay(2);
}

/**
 * @brief Function waits for steps cycles (for loop)
 * @param steps cycles to wait
 */
static void SimpleWait(int steps) {
    for (int i = 0; i < steps; ++i);
}

/**
 * @brief Generates one clock stroke on the EN line L -> H -> L
 */
static void DiplayClkStroke(void) {
    HAL_GPIO_WritePin(DISP_EN_GPIO_Port, DISP_EN_Pin, 1);
    SimpleWait(10);
    HAL_GPIO_WritePin(DISP_EN_GPIO_Port, DISP_EN_Pin, 0);
    SimpleWait(10);
}

/**
 * @brief Sends 4 bit to the display
 * @param data data to send
 */
static void DisplayWriteNibble(DisplayCmd_t data) {
    // output data
    HAL_GPIO_WritePin(DISP_D3_GPIO_Port, DISP_D3_Pin, data & (1 << 3));
    HAL_GPIO_WritePin(DISP_D2_GPIO_Port, DISP_D2_Pin, data & (1 << 2));
    HAL_GPIO_WritePin(DISP_D1_GPIO_Port, DISP_D1_Pin, data & (1 << 1));
    HAL_GPIO_WritePin(DISP_D0_GPIO_Port, DISP_D0_Pin, data & (1 << 0));

    DiplayClkStroke();
}

/**
 * @brief Sends an ASCII character to the display
 * @param character character to send
 */
void DisplayWriteCharacter(char character) {
    // output control signal
    HAL_GPIO_WritePin(DISP_RS_GPIO_Port, DISP_RS_Pin, 1);

    DisplayWriteNibble(character >> 4);
    DisplayWriteNibble(character);
    vTaskDelay(1);
}

/**
 * @brief Sends a string to the display, must be '\0' terminated
 * @param str string
 */
void DisplayWriteString(const char *str) {
    int i = 0;
    while (str[i] != '\0') {
        DisplayWriteCharacter(str[i++]);
    }
}

/**
 * @brief Sends an 8 bit command to the display
 * @param data command
 */
static void DisplayWriteCommand(DisplayCmd_t data) {
    // output control signal
    HAL_GPIO_WritePin(DISP_RS_GPIO_Port, DISP_RS_Pin, 0);

    DisplayWriteNibble(data >> 4);
    DisplayWriteNibble(data);
    vTaskDelay(1);
}

/**
 * @brief Deletes all content from the display and sets cursor to (0,0)
 */
void DisplayClear(void) {
    DisplayWriteCommand(DISP_CLEAR);
    vTaskDelay(2);
}

/**
 * @brief Sets the cursor on the display
 * @param y column (0...3)
 * @param x row (0...19)
 */
void DisplaySetCursor(int y, int x) {
    DisplayWriteCommand(DISP_CURSOR_POS | (y << 5) | x);
}

/**
 * @brief Assembles string an arguments and sends it to the display
 * @param fmt string
 * @param argp arguments
 */
static void vprint(const char *fmt, va_list argp) {
    char string[50];
    if (0 < vsprintf(string, fmt, argp)) // build string
    {
        DisplayWriteString(string);
    }
}

/**
 * @brief printf function for the display
 * @param fmt string
 * @param ... variables
 *
 * Source: https://electronics.stackexchange.com/questions/206113/
 * how-do-i-use-the-printf-function-on-stm32
 */
void DisplayPrintf(const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}
/******************************************************************************/