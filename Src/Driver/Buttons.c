/**
 *******************************************************************************
 * @file    Buttons.c
 * @brief   Polls the button inputs an stores pos edges
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <stm32f1xx_hal.h>
#include <cmsis_os.h>
#include "Buttons.h"

/* Private variables */
static uint8_t prevState;
static uint8_t buttonsPressed;

/* Private functions */

/******************************************************************************/
/**
 * @brief Polls the buttons and sets flag on pos edge
 * @param pvParameters Delay time in ms
 */
void vReadButtons(void *pvParameters) {
    uint32_t delay = (uint32_t) pvParameters;

    while (1) {
        /* Detect positive Transitions (Buttons are active low)*/
        int current = HAL_GPIO_ReadPin(CTRL_C_GPIO_Port, CTRL_C_Pin);
        if (current == BUTTON_PRESSED) {
            if ((prevState & BUTTON_OK) == 0) {
                buttonsPressed |= BUTTON_OK;
            }
            prevState |= BUTTON_OK;
        } else {
            prevState &= ~BUTTON_OK;
        }

        current = HAL_GPIO_ReadPin(CTRL_U_GPIO_Port, CTRL_U_Pin);
        if (current == BUTTON_PRESSED) {
            if ((prevState & BUTTON_UP) == 0) {
                buttonsPressed |= BUTTON_UP;
            }
            prevState |= BUTTON_UP;
        } else {
            prevState &= ~BUTTON_UP;
        }

        current = HAL_GPIO_ReadPin(CTRL_D_GPIO_Port, CTRL_D_Pin);
        if (current == BUTTON_PRESSED) {
            if ((prevState & BUTTON_DOWN) == 0) {
                buttonsPressed |= BUTTON_DOWN;
            }
            prevState |= BUTTON_DOWN;
        } else {
            prevState &= ~BUTTON_DOWN;
        }
        current = HAL_GPIO_ReadPin(CTRL_L_GPIO_Port, CTRL_L_Pin);
        if (current == BUTTON_PRESSED) {
            if ((prevState & BUTTON_LEFT) == 0) {
                buttonsPressed |= BUTTON_LEFT;
            }
            prevState |= BUTTON_LEFT;
        } else {
            prevState &= ~BUTTON_LEFT;
        }

        current = HAL_GPIO_ReadPin(CTRL_R_GPIO_Port, CTRL_R_Pin);
        if (current == BUTTON_PRESSED) {
            if ((prevState & BUTTON_RIGHT) == 0) {
                buttonsPressed |= BUTTON_RIGHT;
            }
            prevState |= BUTTON_RIGHT;
        } else {
            prevState &= ~BUTTON_RIGHT;
        }
        vTaskDelay(delay);
    }
}

/**
 * @brief Clears all flags
 */
void ClearButtons(void) {
    buttonsPressed = 0;
}

/**
 * @brief Reads the buttons pos edge flags and clears them
 * @return Buttons which have been pressed
 */
Button_t ReadButtons(void) {
    Button_t ret = buttonsPressed;
    ClearButtons();
    return ret;
}

/******************************************************************************/