#ifndef BUTTONS_H
#define BUTTONS_H
/**
 *******************************************************************************
 * @file Buttons.h
 *******************************************************************************
 */
/* Constants */
enum {
    BUTTON_PRESSED = 0,
    BUTTON_RELEASED = 1,
};

/* Types */
typedef enum {
    BUTTON_OK = 0x01,
    BUTTON_UP = 0x02,
    BUTTON_DOWN = 0x04,
    BUTTON_LEFT = 0x08,
    BUTTON_RIGHT = 0x10,
} Button_t;

/* Functions */
void vReadButtons(void *pvParameters);
void ClearButtons(void);
Button_t ReadButtons(void);

/******************************************************************************/
#endif /* ifndef BUTTONS_H */
