#ifndef USB_COM_H
#define USB_COM_H
/**
 *******************************************************************************
 * @file UsbCom.h
 *******************************************************************************
 */
/* Constants */

/* Types */

/* Functions */
void UsbPrintf(const char *fmt, ...);
void UsbPrintString(char *str);

/******************************************************************************/
#endif /* ifndef USB_COM_H */
