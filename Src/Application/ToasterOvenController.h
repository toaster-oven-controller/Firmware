#ifndef TOASTER_OVEN_CONTROLLER_H
#define TOASTER_OVEN_CONTROLLER_H
/**
 *******************************************************************************
 * @file ToasterOvenController.h
 *******************************************************************************
 */
/* Constants */

/* Types */

/* Functions */
void vInitToasterOvenController(void *pvParameters);
void ToasterOvenController(void);

/******************************************************************************/
#endif /* ifndef TOASTER_OVEN_CONTROLLER_H */
