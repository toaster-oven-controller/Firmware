/**
 *******************************************************************************
 * @file    ToasterOvenController.c
 * @brief   Main application that controls the toaster oven
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <cmsis_os.h>
#include "ToasterOvenController.h"
#include "stm32f1xx_hal.h"
#include "../Service/StringFunctions.h"
#include "../Service/UserInterface.h"
#include "../Driver/Display.h"
#include "../Driver/UsbCom.h"
#include "../Driver/TemperatureSensor.h"
#include "../Driver/Buttons.h"

/* Private variables */
static TaskHandle_t xDisplayTemperatureHandle = NULL;
static BaseType_t xDisplayTemperature;

static TaskHandle_t xReadButtonsHandle = NULL;
static BaseType_t xReadButtons;

static TaskHandle_t xUserInterfaceHandle = NULL;
static BaseType_t xUserInterface;

/* Private functions */
static void vDisplayTemperature(void *pvParameters);

/******************************************************************************/
/**
 * @brief Initializes Toaster Oven Controller and starts tasks
 */
void vInitToasterOvenController(void *pvParameters) {
    InitDisplay();
    InitUserInterface();
    vTaskDelay(500);

    xReadButtons = xTaskCreate(
            vReadButtons,
            "Read Buttons",
            32,
            (void *) 20, // run every 20ms
            1,
            &xReadButtonsHandle);

    xUserInterface = xTaskCreate(
            vUserInterface,
            "User Interface",
            128,
            (void *) 200, // run every 200ms
            1,
            &xUserInterfaceHandle);

    vTaskDelete(NULL);
}

/******************************************************************************/