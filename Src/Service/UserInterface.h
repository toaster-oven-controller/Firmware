#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H
/**
 *******************************************************************************
 * @file UserInterface.h
 *******************************************************************************
 */
/* Constants */

/* Types */
typedef enum {
    MENU_START,
    MENU_SELECT_PROFILE,
    MENU_RUN_PROFILE,
    MENU_READ_TEMP,
} MenuSelection_t;

typedef struct {
    char name[19];
    MenuSelection_t nextMenu;
} MenuEntry_t;

typedef struct {
    char title[20];
    char firstLine;
    MenuEntry_t entries[3];
    char entriesCount;
    void (*enterFun)(void);
    void (*periodicFun)(void);
    void (*exitFun)(void);
} MenuSystem_t;

/* Functions */
void InitUserInterface(void);
void vUserInterface(void *pvParameters);

/******************************************************************************/
#endif /* ifndef USER_INTERFACE_H */
