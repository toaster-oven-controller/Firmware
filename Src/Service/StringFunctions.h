#ifndef STRING_FUNCTIONS_H
#define STRING_FUNCTIONS_H
/**
 *******************************************************************************
 * @file StringFunctions.h
 *******************************************************************************
 */
/* Constants */

/* Types */

/* Functions */
void FloatToString(char *str, float value, int accuracy);

/******************************************************************************/
#endif /* ifndef STRING_FUNCTIONS_H */
