/**
 *******************************************************************************
 * @file    UserInterface.c
 * @brief   UI presented on the display
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <string.h>
#include "cmsis_os.h"
#include "stm32f1xx_hal.h"
#include "UserInterface.h"
#include "../Service/StringFunctions.h"
#include "../Service/TemperatureController.h"
#include "../Driver/Buttons.h"
#include "../Driver/TemperatureSensor.h"
#include "../Driver/Display.h"

/* Private variables */
static uint8_t cursor = 0;
static MenuSelection_t currentMenu = MENU_START;

/* Private functions */
static void BuildUpMenu(MenuSelection_t selection);
static void UpdateCursor(uint8_t cursor, uint8_t offset);
static void DisplayTemperature(void);

/******************************************************************************/
/* Menu system */

const MenuSystem_t MENU[4] = {
        {
                .title = "Reflow Controller",
                .firstLine = 1,
                .entries = {
                        {
                                .name = "Select Profile",
                                .nextMenu = MENU_SELECT_PROFILE
                        },
                        {
                                .name = "Read Temp",
                                .nextMenu = MENU_READ_TEMP
                        }
                },
                .entriesCount = 2,
                .enterFun = NULL,
                .periodicFun = NULL,
                .exitFun = NULL
        },
        {
                .title = "Select Profile",
                .firstLine = 1,
                .entriesCount = 1,
                .entries = {
                        {
                                .name = "Lead Solder",
                                .nextMenu = MENU_RUN_PROFILE
                        }
                },
                .enterFun = NULL,
                .periodicFun = NULL,
                .exitFun = NULL
        },
        {
                .title = "Run Profile",
                .firstLine = 3,
                .entriesCount = 1,
                .entries = {
                        {
                                .name = "Abort",
                                .nextMenu = MENU_START
                        }
                },
                .enterFun = &StartController,
                .periodicFun = NULL,
                .exitFun = &EndController
        },
        {
                .title = "Read Temperature",
                .firstLine = 1,
                .entriesCount = 0,
                .enterFun = NULL,
                .periodicFun = &DisplayTemperature,
                .exitFun = NULL
        }
};

/******************************************************************************/
/**
 * @brief Displays initial menu
 */
void InitUserInterface(void) {
    BuildUpMenu(MENU_START);
}

/**
 * @brief Updates the User Interface
 * @param pvParameters Delay time in ms
 */
void vUserInterface(void *pvParameters) {
    uint32_t delay = (uint32_t) pvParameters;
    while (1) {
        if (MENU[currentMenu].periodicFun != NULL) {
            MENU[currentMenu].periodicFun();
        }

        Button_t buttons = ReadButtons();
        switch (buttons) {
            case BUTTON_OK:
                /* exit function */
                if (MENU[currentMenu].exitFun != NULL) {
                    MENU[currentMenu].exitFun();
                }

                /* Select next menu */
                if (MENU[currentMenu].entriesCount != 0) {
                    currentMenu = MENU[currentMenu].entries[cursor].nextMenu;
                } else {
                    /* Go back to home */
                    currentMenu = MENU_START;
                }
                cursor = 0;
                BuildUpMenu(currentMenu);

                /* enter function */
                if (MENU[currentMenu].enterFun != NULL) {
                    MENU[currentMenu].enterFun();
                }
                break;
            case BUTTON_UP:
                if (cursor > 0) {
                    cursor--;
                    UpdateCursor(cursor, MENU[currentMenu].firstLine);
                }
                break;
            case BUTTON_DOWN:
                if (cursor < MENU[currentMenu].entriesCount - 1) {
                    cursor++;
                    UpdateCursor(cursor, MENU[currentMenu].firstLine);
                }
                break;
            case BUTTON_LEFT:
                break;
            case BUTTON_RIGHT:
                break;
        }
        vTaskDelay(delay);
    }
}

/**
 * @brief Build new menu screen
 * @param selection Current menu
 */
static void BuildUpMenu(MenuSelection_t selection) {
    DisplayClear();
    DisplayPrintf(MENU[selection].title);
    for (int i = 0; i < MENU[selection].entriesCount; ++i) {
        DisplaySetCursor(MENU[selection].firstLine + i, 0);
        DisplayPrintf(MENU[selection].entries[i].name);
    }

    /* Only display cursor if there are menu entries */
    if (MENU[selection].entriesCount != 0) {
        DisplaySetCursor(MENU[selection].firstLine, 19);
        DisplayWriteCharacter(SYM_ARROW_LEFT);
    }
}

/**
 * @brief Update the cursor on the display
 * @param cursor Position of the cursor (line)
 */
static void UpdateCursor(uint8_t cursor, uint8_t offset) {
    /* Always update all 3 lines */
    for (int i = 0; i < 3; ++i) {
        DisplaySetCursor(offset + i, 19);
        if (i == cursor) {
            DisplayWriteCharacter(SYM_ARROW_LEFT);
        } else {
            DisplayWriteCharacter(' ');
        }
    }
}

/**
 * @brief Displays the current thermocouple temperature on the second line on
 * the display
 */
static void DisplayTemperature(void) {
    float tempThermocouple, tempJunction;
    TempMsg_t tempMsg = ReadTemperature(&tempThermocouple, &tempJunction);
    char strThermocouple[10];
    FloatToString(strThermocouple, tempThermocouple, 100);

    DisplaySetCursor(1, 0);
    DisplayPrintf("Temp: ");

    switch (tempMsg) {
        case TEMP_ERROR_SHORT_GND | TEMP_ERROR_OPEN:
        case TEMP_ERROR_SHORT_GND:
            DisplayPrintf("shorted GND");
            break;
        case TEMP_ERROR_SHORT_VCC | TEMP_ERROR_OPEN:
        case TEMP_ERROR_SHORT_VCC:
            DisplayPrintf("shorted VCC");
            break;
        case TEMP_ERROR_OPEN:
            DisplayPrintf("open       ");
            break;
        default:
            DisplayPrintf(strThermocouple);
            DisplayWriteCharacter(SYM_DEG); // ° symbol
            DisplayPrintf("C      ");
            break;
    }
}

/******************************************************************************/