/**
 *******************************************************************************
 * @file    TemperatureController.c
 * @brief   
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */
#include "cmsis_os.h"
#include "stm32f1xx_hal.h"
#include <string.h>
#include <stdbool.h>
#include "TemperatureController.h"
#include "../Service/StringFunctions.h"
#include "../Driver/TemperatureSensor.h"
#include "../Driver/UsbCom.h"
#include "../Driver/Display.h"
#include "../Driver/Beeper.h"

/* Private variables */
const tempProfile_t profileLead = {
        .T = {100, 150, 183, 230},
        .t = {80,   90,  30,  60},
        .steps = 4,
        .solderT = 220,
        .coolT = 200,
        .name = {
                {"Ramp Soak"},
                {"Soak     "},
                {"Ramp Peak"},
                {"Reflow   "}
        }
};

static int initialT = 0;
static int processStep = 0;
static float t = 0, tStep = 0, tSolder = 0;
static stat_t controllerState = STAT_PROFILE;

const float Kp = 5;
const float Ki = 0;
const float Kd = 50;

static float x, u, e, e_old, e_sum;

static TaskHandle_t xControllerHandle = NULL;
static BaseType_t xController;

/* Private functions */
static void vTemperatureController(void *pvParameters);
static float GetSetValue(tempProfile_t *profile, float timeInStep, int step,
                         float initT);
static void LogTemperature(float setTemp, float temp, float time,
                           TempMsg_t tempMsg);
static void WriteStateTemp(char *state, float T);
static void EnableHeat(bool enable);

/******************************************************************************/

/**
 * @brief Starts the temperature controller
 */
void StartController(void) {
    xController = xTaskCreate(
            vTemperatureController,
            "Temperature Controller",
            200,
            (void *) 500, // run every 500ms
            1,
            &xControllerHandle);
}

/**
 * @brief Deletes the controller task
 */
void EndController(void) {
    vTaskDelete(xControllerHandle);
    StopBeeper();
    EnableHeat(false);
}


/**
* @brief Runs the temperature controller
* @param pvParameters Delay time in ms
*/
static void vTemperatureController(void *pvParameters) {
    uint32_t delay = (uint32_t) pvParameters;
    tempProfile_t *profile = (tempProfile_t *) &profileLead;
    char strT[8];

    float T, tempJunction;
    TempMsg_t tempMsg;
    ReadTemperature(&T, &tempJunction); // discard first faulty temperature
    ReadTemperature(&T, &tempJunction);
    initialT = T;
    t = 0;
    tStep = 0;
    tSolder = 0;
    controllerState = STAT_PROFILE;

    while (1) {
        tempMsg = ReadTemperature(&T, &tempJunction);

        switch (controllerState) {
            case STAT_PROFILE:
                /* PID controller */
                if (tempMsg == TEMP_OK) {
                    x = GetSetValue(profile, tStep, processStep, initialT);
                    e = x - T;
                    u = Kp * e +
                        Ki * e_sum * (float) delay / 1000.0 +
                        Kd * (e - e_old) / (float) delay * 1000.0;

                    e_old = e;
                    e_sum += e;

                    /* Control Heat Element */
                    if (u > 10.0) {
                        EnableHeat(true);
                    } else if (u < 5.0) {
                        EnableHeat(false);
                    }
                }

                /* heating process finished */
                if (processStep == profile->steps) {
                    StopBeeper();
                    StartBeeper(50, 150);
                    EnableHeat(false);
                    controllerState = STAT_COOL;
                    break;
                }

                WriteStateTemp(profile->name[processStep], T);
                break;

            case STAT_COOL:
                WriteStateTemp("Cool Down", T);
                if (T <= profile->coolT) {
                    StopBeeper();
                    controllerState = STAT_FINISHED;
                }
                break;

            case STAT_FINISHED:
                DisplaySetCursor(1, 0);
                DisplayPrintf("Reflow finished     ");
                break;
        }

        /* Log and display current T, t */
        LogTemperature(x, T, t, tempMsg);
        /* show time spend in soldering process */
        if (T >= profile->solderT) {
            DisplaySetCursor(2, 0);
            DisplayPrintf("Solder-time: %ds  ", (int) tSolder);
            tSolder += delay / 1000.0;
        }

        /* update time */
        t += delay / 1000.0;
        tStep += delay / 1000.0;

        /* change process step */
        if (tStep > profile->t[processStep]) {
            tStep = 0;
            processStep++;

            /* enable beeper in reflow state */
            if (processStep == profile->steps-1) {
                StartBeeper(50, 950);
            }
        }

        vTaskDelay(delay);
    }
}

/**
 * @brief Calculates the set value for the controller based on the chosen
 * solder profile (trapezoidal calculation)
 * @param profile solder profile
 * @param timeInStep time spend in current process step
 * @param step current process step
 * @param initT initial temperature
 * @return set temperature
 */
static float GetSetValue(tempProfile_t *profile, float timeInStep, int step,
                         float initT) {
    float T;

    if (step == 0) {
        T = (profile->T[step] - initT)
            / (float) profile->t[step] * timeInStep
            + initT;
    } else {
        T = (float) (profile->T[step] - profile->T[step-1])
            / (float) profile->t[step] * timeInStep
            + (float) profile->T[step-1];
    }
    return T;
}

/**
 * @brief Log temperature on the virtual COM Port xml-style
 * @param temp temperature
 * @param time time since start of soldering process
 * @param tempMsg message from temperature sensor
 */
static void LogTemperature(float setTemp, float temp, float time,
                           TempMsg_t tempMsg) {
    char strThermocouple[8];
    char strSetTemp[8];
    char strTime[5];
    char strLog[60];
    FloatToString(strThermocouple, temp, 100);
    FloatToString(strSetTemp, setTemp, 100);
    FloatToString(strTime, time, 100);

    switch (tempMsg) {
        case TEMP_ERROR_SHORT_GND | TEMP_ERROR_OPEN:
        case TEMP_ERROR_SHORT_GND:
            strcpy(strLog, "<temp><e>shorted to GND</e></temp>\n");
            break;
        case TEMP_ERROR_SHORT_VCC | TEMP_ERROR_OPEN:
        case TEMP_ERROR_SHORT_VCC:
            strcpy(strLog, "<temp><e>shorted to VCC</e></temp>\n");
            break;
        case TEMP_ERROR_OPEN:
            strcpy(strLog, "<temp><e>open Leads</e></temp>\n");
            break;
        default:
            strcpy(strLog, "<temp><Ts>");
            strcat(strLog, strSetTemp);
            strcat(strLog, "</Ts><T>");
            strcat(strLog, strThermocouple);
            strcat(strLog, "</T><t>");
            strcat(strLog, strTime);
            strcat(strLog, "</t></temp>\n");
            break;
    }
    UsbPrintString(strLog);
}

/**
 * @brief Displays the current state and temperature on line 2 on the display
 * @param state string of state name
 * @param T temperature
 */
static void WriteStateTemp(char *state, float T) {
    char strT[8];

    DisplaySetCursor(1, 0);
    DisplayPrintf("%s", state);
    DisplaySetCursor(1, 10);
    FloatToString(strT, T, 100);
    DisplayPrintf("%s", strT);
    DisplayWriteCharacter(SYM_DEG);
    DisplayPrintf("C");
}

/**
 * @brief Controls the heat element
 * @param enable turn on/off heat element
 */
static void EnableHeat(bool enable) {
    if (enable == true) {
        HAL_GPIO_WritePin(Heat1_GPIO_Port, Heat1_Pin,
                          GPIO_PIN_SET);
        DisplaySetCursor(1, 19);
        DisplayWriteCharacter(SYM_DOT);
    } else {
        HAL_GPIO_WritePin(Heat1_GPIO_Port, Heat1_Pin,
                          GPIO_PIN_RESET);
        DisplaySetCursor(1, 19);
        DisplayWriteCharacter(' ');
    }
}

/******************************************************************************/