#ifndef TEMPERATURE_CONTROLLER_H
#define TEMPERATURE_CONTROLLER_H
/**
 *******************************************************************************
 * @file TemperatureController.h
 *******************************************************************************
 */
/* Constants */
typedef struct {
    int T[4];
    int t[4];
    int steps;
    int solderT;
    int coolT;
    char name[4][20];
} tempProfile_t;

typedef enum {STAT_PROFILE, STAT_COOL, STAT_FINISHED} stat_t;


/* Types */

/* Functions */
void StartController(void);
void EndController(void);

/******************************************************************************/
#endif /* ifndef TEMPERATURE_CONTROLLER_H */
