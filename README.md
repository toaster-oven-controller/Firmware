# Toaster Oven Controller Firmware
The aim of this project is to use a toaster oven as reflow oven to solder electronical components. Therefore, a microcontroller (STM32F103) is used to control the temperature in the oven by switching the heat elements with an solid state relay.

## Prerequisites
The firmware developed using JetBrains CLion as IDE (with the [STM32CubeMX plugin](https://plugins.jetbrains.com/plugin/10115-openocd--stm32cubemx-support-for-arm-embedded-development)) and the GCC-ARM-None-EABI compiler. In addition STM32CubeMX was used for the hardware abstraction layer (HAL-Layer).

## Current State
Reflow soldering with lead solder was successfully tested, thus the firmware is not beeing developed any further.

### Features
* Simple menu on a 4x20 character display
* Reflow profile for lead solder paste
* Display of current temperature
* USB (Vitual COM port)
* FreeRTOS

## Basic Structure
There are a lot of files created by STM32CubeMX. The user code files are located in the _Src_ directory.
The _Src_ contains:
* main.c: starts the toaster oven controller task
* Application/: Main application
* Service/: Services for the application layer, e.g. UserInterface
* Driver/: Drivers for the hardware components

## Hardware Design
Information about the hardware design can be found on [CircuitMaker](https://workspace.circuitmaker.com/Projects/Details/StefanLuethi/Toaster-Oven-Controller).
